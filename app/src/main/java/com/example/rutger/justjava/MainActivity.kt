package com.example.rutger.justjava

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView

/**
 * This app displays an order form to order coffee.
 */
class MainActivity : AppCompatActivity() {
    var orderQuantity = 0
    val UNIT_PRICE = 2.20
    var totalPrice = 0.00

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    /**
     * This method is called when the order button is clicked.
     */
    fun submitOrder(view: View) {
        totalPrice = orderQuantity*UNIT_PRICE
        val priceMessage = "Total: $" + totalPrice +"\nThank You!"
        displayMessage(priceMessage)
    }

    fun incrementOrderQuantity(view: View) {
        orderQuantity = orderQuantity+1
        display(orderQuantity)
    }

    fun decrementOrderQuantity(view: View) {
        if (orderQuantity > 0) {
            orderQuantity = orderQuantity-1
            display(orderQuantity)
        }

    }
    /**
     * This method displays the given quantity value on the screen.
     */
    private fun display(number: Int) {
        val quantityTextView = findViewById<View>(R.id.quantity_text_view) as TextView
        quantityTextView.text = number.toString()
    }

    /**
     * This method displays the given text on the screen.
     */
    private fun displayMessage(message: String) {
        val priceTextView = findViewById<View>(R.id.price_text_view) as TextView
        priceTextView.text = message
    }
}